package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.mapper.NoteMapper;
import com.udacity.jwdnd.course1.cloudstorage.model.Note;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteService {

    NoteMapper noteMapper;

    public NoteService(NoteMapper noteMapper) {
        this.noteMapper = noteMapper;
    }

    public Integer addNoteForUser(Note note) {
        int rows = noteMapper.addNote(note);
        if (rows != 0) {
            return note.getNoteId();
        }
        return null;
    }

    public List<Note> getAllNotesForUser(int userId) {
        return noteMapper.fetchNotesforUser(userId);
    }

    public Integer updateNote(Note updatedNote) {
        return noteMapper.updateNote(updatedNote);
    }

    public Integer getUserforNote(int noteId) {
        return noteMapper.getUserForNote(noteId);
    }

    public Integer deleteNote(Integer noteId) {
        return noteMapper.deleteNote(noteId);
    }

    public Note getNoteforId(int nodeId) {
        return noteMapper.getNoteforId(nodeId);
    }

    /**
     * Fetches the first Note with the given title
     * if the title is unique there is only one note
     * @param title unique title
     * @return Note or Null if nothing present
     */
    public Note getUniqueNoteForTitle(String title) {
        List<Note> notes = noteMapper.getNotesForTitle(title);
        if (notes.size() > 0) {
            return notes.get(0);
        }
        return null;
    }
}
