package com.udacity.jwdnd.course1.cloudstorage.mapper;

import com.udacity.jwdnd.course1.cloudstorage.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UsersMapper {

    @Select("SELECT * from users " +
            "WHERE username=#{username}")
    User getUser(String username);

    @Insert("INSERT INTO users " +
            "(username, salt, password, firstname,lastname) " +
            "VALUES(#{userName}, #{salt}, #{password}, #{firstName}, #{lastName})")
    @Options(useGeneratedKeys = true,
            keyColumn = "userid",
            keyProperty = "userId")
    Integer addUser(User user);

    @Delete("DELETE FROM users " +
            "WHERE username=#{username)}")
    int deleteUser(String username);
}
