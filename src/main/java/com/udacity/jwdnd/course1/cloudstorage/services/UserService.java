package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.helpers.PasswordHelpers;
import com.udacity.jwdnd.course1.cloudstorage.mapper.UsersMapper;
import com.udacity.jwdnd.course1.cloudstorage.model.User;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    UsersMapper usersMapper;
    HashService hashService;

    public UserService(UsersMapper usersMapper, HashService hashService) {
        this.usersMapper = usersMapper;
        this.hashService = hashService;
    }

    public User getUser(String userName) {
        return usersMapper.getUser(userName);
    }

    public Integer addUser(User user) {
        var salt = PasswordHelpers.getSalt();
        var encodedPassword = hashService.getHashedValue(user.getPassword(), salt);
        return usersMapper.addUser(
                new User(null,
                        user.getUserName(),
                        user.getFirstName(),
                        user.getLastName(),
                        salt,
                        encodedPassword)
        );


    }

    public int deleteUser(String userName) {
        return usersMapper.deleteUser(userName);
    }

}
