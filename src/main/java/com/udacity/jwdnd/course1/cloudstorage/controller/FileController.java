package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.model.File;
import com.udacity.jwdnd.course1.cloudstorage.services.FileService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.io.FileNotFoundException;
import java.io.IOException;

@Controller
@ControllerAdvice
@RequestMapping("/files")
public class FileController {

    Logger logger = LoggerFactory.getLogger(FileController.class);

    FileService fileService;
    UserService userService;

    public FileController(FileService fileService, UserService userService) {
        this.fileService = fileService;
        this.userService = userService;
    }

    @GetMapping("/delete")
    public RedirectView deleteFile(@RequestParam Integer id,
                                   RedirectAttributes attributes,
                                   Authentication authentication) {

        final File file = fileService.getFileById(id);
        final String fileName = "";

        // check if file exists
        if (file == null) {
            attributes.addFlashAttribute("errorMsg", "No file with id=" + id + " found :-(");
            return new RedirectView("/home");
        }

        //check permission
        if (!file.getUserId().equals(userService.getUser(authentication.getName()).getUserId())) {
            attributes.addFlashAttribute("errorMsg", "Sorry, that is not your file!");
            return new RedirectView("/home");
        }

        if (fileService.deleteFilyById(id) == 0) {
            attributes.addFlashAttribute("errorMsg", "No File deleted :-(");
        } else {
            attributes.addFlashAttribute("successMsg", "File " +
                    fileName +
                    " successfully deleted");
        }

        return new RedirectView("/home");
    }

    @GetMapping("/view")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@RequestParam Integer id,
                                            Model model,
                                            Authentication authentication)
            throws FileNotFoundException {

        File file = fileService.getFileById(id);

        if (file == null) {
            throw new FileNotFoundException("No file found with id=" + id);
        }

        if (!file.getUserId().equals(userService.getUser(authentication.getName()).getUserId())) {
            throw new PermissionDeniedDataAccessException(
                    "You have no permission to access the file!", null);
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(file.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + file.getFileName() + "\"")
                .body(new ByteArrayResource(file.getFileData()));
    }


    @SuppressWarnings("finally")
    @PostMapping
    public RedirectView fileUpload(MultipartFile fileUpload,
                                   RedirectAttributes attributes,
                                   Authentication authentication) throws IOException {
        //getUserId
        Integer userId = userService.getUser(authentication.getName()).getUserId();

        //check if files uploaded
        if (fileUpload.isEmpty()) {
            attributes.addFlashAttribute("errorMsg", "Select a file first !");
            return new RedirectView("/home");
        }

        //check if file already exists
        if (fileService.getFileByName(fileUpload.getOriginalFilename(), userId) != null) {
            attributes.addFlashAttribute("errorMsg", "File already exists!");
            return new RedirectView("/home");
        }

        try {

            Integer saveFile = fileService.saveFile(fileUpload, userId);
            attributes.addFlashAttribute("successMsg",
                    "Successfully uploaded file with Id: " + saveFile);

        } catch (IOException e) {

            attributes.addFlashAttribute("errorMsg", "There was an unexpected error :-(");
            logger.error("Error Uploading File: " + e.getMessage());

        } finally {

            return new RedirectView("/home");
        }

    }

    @ExceptionHandler({FileSizeLimitExceededException.class, IllegalStateException.class})
    public RedirectView handleMaxFileUploadException(RedirectAttributes attributes) {
        attributes.addFlashAttribute("errorMsg", "File excceeds Upload Limit");
        return new RedirectView("/home");
    }

    @ExceptionHandler(FileNotFoundException.class)
    public RedirectView handleFileNotFoundException(RedirectAttributes attributes, Exception exception) {
        attributes.addFlashAttribute("errorMsg", exception.getMessage());
        return new RedirectView("/home");
    }

    @ExceptionHandler(PermissionDeniedDataAccessException.class)
    public RedirectView handlePermissionDeniedDataAccessException(RedirectAttributes attributes, Exception exception) {
        attributes.addFlashAttribute("errorMsg", exception.getMessage());
        return new RedirectView("/home");
    }
}
