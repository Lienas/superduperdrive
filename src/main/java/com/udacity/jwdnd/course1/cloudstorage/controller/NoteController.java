package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.model.Note;
import com.udacity.jwdnd.course1.cloudstorage.services.NoteService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping("/notes")
public class NoteController {

    private NoteService noteService;
    private UserService userService;
    private List<Note> notes;

    public NoteController(NoteService noteService, UserService userService) {
        this.noteService = noteService;
        this.userService = userService;
    }

    @GetMapping
    public RedirectView getHome() {
        return new RedirectView("/home");
    }

    @PostMapping
    public RedirectView UpdateOrCreateNote(@ModelAttribute Note note,
                                           RedirectAttributes attributes,
                                           Authentication authentication) {

        final Integer authUserId =
                userService.getUser(authentication.getName()).getUserId();
        Integer noteId = note.getNoteId();

        // create note
        if (noteId == null) {
            note.setUserId(authUserId);
            //Create a new note
            noteId = noteService.addNoteForUser(note);

            if (noteId > 0) {
                attributes.addFlashAttribute("successMsg",
                        "Note [" + noteId + "] was created successfully!");
            } else {
                attributes.addFlashAttribute("errorMsg",
                        "There was an error adding the note! Please try again.");
            }
        } else {
            //update note
            //check if user has created this note
            if (noteService.getUserforNote(noteId) != authUserId) {
                attributes.addFlashAttribute("errorMsg",
                        "You are not authorized to modify this note!");
            } else {

                if (noteService.updateNote(note) != 0) {
                    attributes.addFlashAttribute("successMsg",
                            "Note was updated successfully!");
                } else {
                    attributes.addFlashAttribute("errorMsg",
                            "There was an error updating the note! Please try again.");
                }
            }
        }

        return new RedirectView("/home#nav-notes-tab");
    }

    @GetMapping("/delete")
    public RedirectView deleteNote(@RequestParam int id,
                                   RedirectAttributes attributes,
                                   Authentication authentication) {

        final Integer authUserId =
                userService.getUser(authentication.getName()).getUserId();
        final Note note = noteService.getNoteforId(id);


        if (note != null) {
            //check if user has created this note
            if (noteService.getUserforNote(id) != authUserId) {
                attributes.addFlashAttribute("errorMsg",
                        "You are not authorized to delete this note!");
            } else {
                if (noteService.deleteNote(id) > 0) {
                    attributes.addFlashAttribute("successMsg",
                            "Successfully deleted Note with id " + id);
                } else {
                    attributes.addFlashAttribute("errorMsg",
                            "Something went wrong. Message not deleted");
                }
            }
        } else {
            attributes.addFlashAttribute("errorMsg",
                    "No Note with ID= " + id + " available!");
        }

        return new RedirectView("/home#nav-notes-tab");

    }
}
