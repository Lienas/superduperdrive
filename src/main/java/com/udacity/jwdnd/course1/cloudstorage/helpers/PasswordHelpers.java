package com.udacity.jwdnd.course1.cloudstorage.helpers;

import java.security.SecureRandom;
import java.util.Base64;

public class PasswordHelpers {

    /**
     *  generate a random 16 Byte
     *  BasedEncodesString
     *  @return String
     */
    public static String getSalt(){

        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);

        return Base64.getEncoder().encodeToString(salt);
    }


    /**
     * generate a random key for encryption
     * @return random bade64 encoded key
     */
    public static String getEncryptionKey() {
        SecureRandom random = new SecureRandom();
        byte[] key = new byte[16];
        random.nextBytes(key);
        String encodedKey = Base64.getEncoder().encodeToString(key);
        return encodedKey;
    }
}
