package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.model.Note;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class NoteServiceTest {

    @Autowired
    private NoteService noteService;

    @Test
    void getUniqueNoteForTitle() {
        final Note uniqueNoteForTitle = noteService.getUniqueNoteForTitle("i am not there- hopefully");
        Assertions.assertNull(uniqueNoteForTitle);
    }
}
