package com.udacity.jwdnd.course1.cloudstorage.mapper;

import com.udacity.jwdnd.course1.cloudstorage.model.Credential;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CredentialMapper {
    @Select("Select  MAX(credentialid) " +
            "FROM credentials " +
            "WHERE userid=#{userId}")
    Integer getMaxCredentialIdForUser(Integer userId);

    @Insert("INSERT INTO credentials " +
            "(url, username, password, userid, key) " +
            "VALUES (#{url}, #{userName}, #{password}, #{userId}, #{key})")
    @Options(useGeneratedKeys = true,
            keyProperty = "credentialId",
            keyColumn = "credentialid")
    Integer addCredential(Credential credential);

    @Select("SELECT * FROM credentials " +
            "WHERE userId=#{userId}")
    List<Credential> getCredentialsForUser(Integer userId);

    @Select("SELECT key FROM credentials " +
            "WHERE credentialid=#{credentialId}")
    String getKeyForCredential(Integer credentialId);

    @Select("SELECT * FROM credentials " +
            "WHERE credentialid=#{credentialId}")
    Credential getCredentialById(Integer credentialId);

    @Delete("DELETE from credentials " +
            "WHERE credentialid=#{credentialId}")
    Integer deleteCredentialById(Integer credentialId);

    @Update("UPDATE credentials SET " +
            "username=#{userName}, " +
            "url=#{url}, " +
            "key=#{key}, " +
            "password=#{password} " +
            "WHERE credentialid=#{credentialId}" )
    int updateCredential(Credential credential);
}
