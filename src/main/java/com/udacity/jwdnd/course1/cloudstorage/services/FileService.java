package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.mapper.FileMapper;
import com.udacity.jwdnd.course1.cloudstorage.model.File;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class FileService {
    FileMapper fileMapper;

    public FileService(FileMapper fileMapper) {
        this.fileMapper = fileMapper;
    }

    public Integer saveFile(MultipartFile multipartFile, Integer userId) throws IOException {

        File file = new File(multipartFile.getOriginalFilename(),
                multipartFile.getContentType(),
                String.valueOf(multipartFile.getSize()),
                userId,
                multipartFile.getBytes()
                );

         fileMapper.saveFile(file);
         return file.getFileId();
    }

    public File getFileByName(String name, Integer userId) {
        return fileMapper.getFileByName(name, userId);
    }

    public File getFileById(Integer fileId) {
        return fileMapper.selectFileById(fileId);
    }

   public List<File> getAllFilesForUser(Integer userId){
        return fileMapper.getAllFilesForUser(userId);
    }

    public Integer deleteFilyById(Integer fileId){
        return fileMapper.deleteFilyById(fileId);
    }
}
