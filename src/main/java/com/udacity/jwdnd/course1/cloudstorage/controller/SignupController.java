package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.model.User;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/signup")
public class SignupController {

    UserService userService;

    public SignupController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String SignupView(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("errorMsg",null);
        return "signup";
    }

    @PostMapping
    public String SignUpUser(@ModelAttribute User user, Model model) {

        //check for existing user
        //maybe throwing an exception is cleaner?
        if (userService.getUser(user.getUserName()) != null) {
            model.addAttribute("errorMsg",
                    "User already exists");
            return "signup";
        }

        var userId = userService.addUser(user);

        if (userId == null) {
            model.addAttribute("errorMsg",
                    "User not created! Please try again!");
            return "signup";
        }

        model.addAttribute("success", true);
        return "signup";

    }

}
