package com.udacity.jwdnd.course1.cloudstorage.pagemodels;

import com.udacity.jwdnd.course1.cloudstorage.model.Note;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class HomePage {

    private final WebDriverWait wait;
    private final JavascriptExecutor js;

    @FindBy(id = "logoutBtn")
    private WebElement logOutBtn;

    @FindBy(id = "addNoteBtn")
    private WebElement addNoteBtn;

    @FindBy(id = "note-id")
    WebElement noteIdHiddenField;

    @FindBy(id = "note-title")
    WebElement noteTitleField;

    @FindBy(id = "note-description")
    private WebElement noteDescField;

    @FindBy(id = "noteSubmit")
    private WebElement submitNoteBtn;

    @FindBy(id = "nav-notes-tab")
    private WebElement activateNoteTabBtn;

    @FindBy(id = "noteTable")
    private WebElement noteTable;

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 2);
        js = (JavascriptExecutor) driver;
    }

    public void logout() {
        js.executeScript("arguments[0].click()", logOutBtn);
    }

    // the following 2 methods could be refactored :-)
    public void addNewNote(Note note) {
        js.executeScript("arguments[0].click()", activateNoteTabBtn);
        js.executeScript("arguments[0].click()", addNoteBtn);

        addOrUpdateNote(note);
    }

    public void updateNote(Note note) {

        js.executeScript("arguments[0].click()", activateNoteTabBtn);
        clickButtonById("note-btn-edit-" + note.getNoteId());

        addOrUpdateNote(note);
    }

    private void addOrUpdateNote(Note note) {
        js.executeScript("arguments[0].value='" + note.getNoteTitle() + "'", noteTitleField);
        js.executeScript("arguments[0].value='" + note.getNoteDescription() + "'", noteDescField);
        js.executeScript("arguments[0].click()", submitNoteBtn);
    }

    public void clickButtonById(String id) {
        WebElement element = wait.until(driver -> driver.findElement(By.id(id)));
        js.executeScript("arguments[0].click()", element);
    }

    public String getInnerHTMLForId(String id) {
        return wait.until(driver -> driver.findElement(By.id(id)).getAttribute("innerHTML"));
    }

    public Integer getIdFromNotesTable() {
        //get all child-Elements of the table
        List<WebElement> rows = noteTable
                .findElement(By.tagName("tbody"))
                .findElements(By.tagName("tr"));

        if (rows.size() == 0) return null;

        //get random element of list
        int randomElement = (int) (Math.random() * (rows.size() - 1.0));
        String idAttr = rows.get(randomElement).getAttribute("id");

        //parse id from id-string notes-[id]
        idAttr = idAttr.substring(idAttr.indexOf('-') + 1);

        return Integer.parseInt(idAttr);
    }
}
