package com.udacity.jwdnd.course1.cloudstorage.pagemodels;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private final WebDriverWait wait;

    @FindBy(id = "inputUsername")
    private WebElement usernameField;

    @FindBy(id = "inputPassword")
    private WebElement passwordField;

    @FindBy(id = "loginBtn")
    private WebElement submitBtn;

    @FindBy(id="errorMsg")
    private WebElement errorMsg;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        wait = new WebDriverWait(driver,10);
    }

    public void login(String userName, String password) {
        wait.until(ExpectedConditions
                .elementToBeClickable(usernameField))
                .sendKeys(userName);
        wait.until(ExpectedConditions
                .elementToBeClickable(passwordField))
                .sendKeys(password);
        submitBtn.click();
    }

    public String getErrorMsg(){
        return wait.until(ExpectedConditions.visibilityOf(errorMsg)).getText();
    }

}
