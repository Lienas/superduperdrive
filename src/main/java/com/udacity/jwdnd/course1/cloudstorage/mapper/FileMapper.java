package com.udacity.jwdnd.course1.cloudstorage.mapper;

import com.udacity.jwdnd.course1.cloudstorage.model.File;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface FileMapper {
    @Insert("INSERT INTO files " +
            "(filename, contenttype, filesize, userid, filedata) " +
            "VALUES(#{fileName}, #{contentType}, #{fileSize}, #{userId}, #{fileData})")
    @Options(useGeneratedKeys = true, keyProperty = "fileId", keyColumn = "fileid")
    Integer saveFile(File file);

    @Select("Select * FROM files " +
            "WHERE filename=#{fileName} AND userid=#{userId}")
    File getFileByName(String fileName, Integer userId);

    @Select("SELECT * FROM files " +
            "WHERE userid = #{userId}")
    List<File> getAllFilesForUser(Integer userId);

    @Select("SELECT * FROM files " +
            "WHERE fileid=#{fileId}")
    File selectFileById(Integer fileId);

    @Delete("DELETE FROM files " +
            "WHERE fileid=#{fileId} ")
    Integer deleteFilyById(Integer filedId);
}
