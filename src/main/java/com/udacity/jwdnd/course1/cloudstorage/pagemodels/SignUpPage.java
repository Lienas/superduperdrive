package com.udacity.jwdnd.course1.cloudstorage.pagemodels;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignUpPage {
    @FindBy(id = "inputUserName")
    private WebElement usernameField;

    @FindBy(id = "inputFirstName")
    private WebElement firstnameField;

    @FindBy(id = "inputLastName")
    private WebElement lastnameField;

    @FindBy(id = "inputPassword")
    private WebElement passwordField;

    @FindBy(id = "signUpBtn")
    private WebElement submitBtn;

    @FindBy(id = "successMsg")
    private WebElement successMsg;

    @FindBy(id = "backToLogin")
    private WebElement backToLoginLink;

    private final WebDriverWait wait;
    private final JavascriptExecutor js;

    public SignUpPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 1000);
        js = (JavascriptExecutor) driver;
    }
    /**
     * fill Signup Form
     *
     * @param userName  = login-name
     * @param firstName = First Name of User
     * @param lastName  = Last Name of User
     * @param password  = Password
     */
    public void signUp(String userName, String firstName, String lastName, String password) {
        js.executeScript("arguments[0].value='" + firstName + "';", firstnameField);
        js.executeScript("arguments[0].value='" + lastName + "';", lastnameField);
        js.executeScript("arguments[0].value='" + userName + "';", usernameField);
        js.executeScript("arguments[0].value='" + password + "';", passwordField);
    }

    public String getSuccessMessage() {
        return wait.until(webdriver ->
                webdriver.findElement(By.id("successMsg")).getText());
    }

    public String getErrorMessage() {
        return wait.until(webdriver ->
                webdriver.findElement(By.id("errorMsg"))).getText();
    }

    public void submit() {
        js.executeScript("arguments[0].click();", submitBtn);
    }
    public void backToLogin() {
        js.executeScript("arguments[0].click();", backToLoginLink);
    }
}
