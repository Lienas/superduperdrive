package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.helpers.PasswordHelpers;
import com.udacity.jwdnd.course1.cloudstorage.mapper.CredentialMapper;
import com.udacity.jwdnd.course1.cloudstorage.model.Credential;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CredentialService {
    CredentialMapper credentialMapper;
    EncryptionService encryptionService;

    public CredentialService(CredentialMapper credentialMapper,
                             EncryptionService encryptionService) {
        this.credentialMapper = credentialMapper;
        this.encryptionService = encryptionService;
    }

    public Integer getCredentialId(Integer userId) {
        return credentialMapper.getMaxCredentialIdForUser(userId);
    }

    public Integer addCredential(Credential credential) {

        encryptPassword(credential);
        credential.setUserId(credential.getUserId());

        return credentialMapper.addCredential(credential);
    }

    public void encryptPassword(Credential credential) {
        String encodedKey = PasswordHelpers.getEncryptionKey();
        String encPassword = encryptionService
                .encryptValue(credential.getPassword(), encodedKey);

        credential.setPassword(encPassword);
        credential.setKey(encodedKey);
    }

    public List<Credential> getCredentialsForUser(Integer userId) {
        return credentialMapper.getCredentialsForUser(userId);
    }

    public String getKeyForCredential(Integer credentialId) {
        return credentialMapper.getKeyForCredential(credentialId);
    }

    public Integer deleteCredential(Integer credentialId) {
        return credentialMapper.deleteCredentialById(credentialId);
    }

    public Credential getCredentialById(Integer credentialId) {
        return credentialMapper.getCredentialById(credentialId);
    }

    public void decryptPassword(Credential credential) {
        String decryptedPassword = encryptionService
                .decryptValue(credential.getPassword(), credential.getKey());

        credential.setPassword(decryptedPassword);
    }

    public int updateCredential(Credential credential) {
        encryptPassword(credential);
        return credentialMapper.updateCredential(credential);
    }
}
