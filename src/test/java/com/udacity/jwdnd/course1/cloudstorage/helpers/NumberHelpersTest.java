package com.udacity.jwdnd.course1.cloudstorage.helpers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;

class NumberHelpersTest {

    Logger logger = LoggerFactory.getLogger(NumberHelpersTest.class);

    @Test
    void getRandomNumber() {
        Integer testVal = 10;
        Integer iterations = 150;
        Integer[] result = new Integer[iterations];

        for (int i = 0; i < iterations; i++) {
            result[i] = NumberHelpers.getRandomNumber(testVal);
            logger.debug(String.format("Result Iteration %d = %d", i, result[i]));
        }
        Assertions.assertTrue(true);

    }
}
