package com.udacity.jwdnd.course1.cloudstorage.helpers;

public class NumberHelpers {
    /**
     * Get a random number in a given range
     * @param start smallest number
     * @param end biggest number
     * @return random number
     */
    public static Integer getRandomNumber(Integer start, Integer end) {
        int randomElement = (int) (Math.random() * (end)) + start;
        return randomElement;
    }

    /**
     * Get a random number from between 0 an end
     * @param end maximum
     * @return random number
     */
    public static Integer getRandomNumber(Integer end) {
        return getRandomNumber(0, end);
    }
}
