package com.udacity.jwdnd.course1.cloudstorage;

import com.udacity.jwdnd.course1.cloudstorage.helpers.NumberHelpers;
import com.udacity.jwdnd.course1.cloudstorage.model.Credential;
import com.udacity.jwdnd.course1.cloudstorage.model.Note;
import com.udacity.jwdnd.course1.cloudstorage.model.User;
import com.udacity.jwdnd.course1.cloudstorage.pagemodels.CredentialPage;
import com.udacity.jwdnd.course1.cloudstorage.pagemodels.LoginPage;
import com.udacity.jwdnd.course1.cloudstorage.pagemodels.HomePage;
import com.udacity.jwdnd.course1.cloudstorage.pagemodels.SignUpPage;
import com.udacity.jwdnd.course1.cloudstorage.services.CredentialService;
import com.udacity.jwdnd.course1.cloudstorage.services.EncryptionService;
import com.udacity.jwdnd.course1.cloudstorage.services.NoteService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CloudStorageApplicationTests {

    private static String userName = "test";
    private static String password = "test";
    private static String firstName = "Max";
    private static String lastName = "Mustermann";
    private String baseUrl;

    @LocalServerPort
    private int port;

    private WebDriver driver;

    //<editor-fold desc="dependency injection">
    @Autowired
    private UserService userService;

    @Autowired
    private NoteService noteService;

    @Autowired
    private CredentialService credentialService;

    @Autowired
    private EncryptionService encryptionService;

    //</editor-fold>

    //<editor-fold desc="prepare and end">
    @BeforeAll
    static void beforeAll() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    public void beforeEach() {
        this.driver = new ChromeDriver();
        baseUrl = "http://localhost:" + this.port;
    }

    @AfterEach
    public void afterEach() {
        if (this.driver != null) {
            driver.quit();
        }
    }
    //</editor-fold>

    //<editor-fold desc="Test Page Access 1-2">
    @Test
    @Order(1)
    public void getLoginPage() {
        driver.get(baseUrl + "/login");
        Assertions.assertEquals("Login", driver.getTitle());
    }

    @Test
    @Order(2)
    public void getSignupPage() {
        driver.get(baseUrl + "/signup");
        Assertions.assertEquals("Sign Up", driver.getTitle());
    }
    //</editor-fold>

    //<editor-fold desc="Test Signup and Login-Logout 3-7">
    @Test
    @Order(3)
    public void signUpUser() throws InterruptedException {
        driver.get(baseUrl + "/signup");
        SignUpPage signUpPage = new SignUpPage(driver);

        // check if test-user exists and delete
        // this is necessary, because i am using a file based database
        // during testing to enable usage of IDE integrated db-tools for inspection
        if (userService.getUser(userName) != null) {
            userService.deleteUser(userName);
        }

        signUpPage.signUp(userName, "Max", "Mustermann", password);
        signUpPage.submit();

        Assertions.assertNotNull(signUpPage.getSuccessMessage());
        Assertions.assertNotNull(userService.getUser(userName));

    }

    @Test
    @Order(4)
    public void rejectSignUpSameUser() {

        driver.get(baseUrl + "/signup");
        SignUpPage signUpPage = new SignUpPage(driver);

        signUpPage.signUp(userName, "Max2", "Mustermann2", password);
        signUpPage.submit();

        Assertions.assertNotNull(signUpPage.getErrorMessage());

    }

    @Test
    @Order(5)
    public void navigateToLogin() {
        driver.get(baseUrl + "/signup");
        SignUpPage signUpPage = new SignUpPage(driver);

        signUpPage.backToLogin();
        Assertions.assertEquals(baseUrl + "/login", driver.getCurrentUrl());
    }

    @Test
    @Order(6)
    public void loginLogoutExistingUser() {

        login();

        HomePage homePage = new HomePage(driver);
        homePage.logout();

        Assertions.assertEquals("Login", driver.getTitle());
        //try to navigate to home
        driver.get(baseUrl + "/home");

        Assertions.assertEquals("Login", driver.getTitle());

    }

    @Test
    @Order(7)
    public void loginFailure() {
        //log User in
        driver.get(baseUrl + "/login");
        LoginPage loginPage = new LoginPage(driver);
        var wrongPassword = "wrongPassword";

        loginPage.login(userName, wrongPassword);
        Assertions.assertEquals(baseUrl + "/login?error", driver.getCurrentUrl());

    }
    //</editor-fold>

    //<editor-fold desc="Test Note CRUD 8-10">
    @Test
    @Order(8)
    public void createNote() {

        login();

        HomePage homePage = new HomePage(driver);

        Note note = createTestNote();

        // call modal form and submit
        homePage.addNewNote(note);
        // get new note
        Note newNote = noteService.getUniqueNoteForTitle(note.getNoteTitle());

        Assertions.assertNotNull(newNote);
        Assertions.assertEquals(newNote.getNoteTitle(), homePage.getInnerHTMLForId("note-title-" + newNote.getNoteId()));
        Assertions.assertEquals(newNote.getNoteDescription(), homePage.getInnerHTMLForId("note-desc-" + newNote.getNoteId()));
    }

    @Test
    @Order(9)
    public void updateNote() {
        login();

        HomePage homePage = new HomePage(driver);
        //get existing Note
        List<Note> existingNotes = noteService
                .getAllNotesForUser(userService.getUser(userName).getUserId());

        if (existingNotes.size() == 0) {
            //notes create one
            Note testNote = createTestNote();
            noteService.addNoteForUser(testNote);
            //try again
            existingNotes = noteService
                    .getAllNotesForUser(userService.getUser(userName).getUserId());

            //refresh the site, otherwise the new note is not visible
            driver.navigate().refresh();
        }

        Note firstNote = existingNotes.get(0);

        // create a new unique title
        String title = makeUniqueTitle();
        firstNote.setNoteTitle(title);
        firstNote.setNoteDescription("I was changed at " + new Date());

        homePage.updateNote(firstNote);

        Assertions.assertEquals(firstNote.getNoteTitle(),
                homePage.getInnerHTMLForId("note-title-" + firstNote.getNoteId()));
        Assertions.assertEquals(firstNote.getNoteDescription(),
                homePage.getInnerHTMLForId("note-desc-" + firstNote.getNoteId()));

    }

    @Test
    @Order(10)
    public void deleteNote() {
        login();

        HomePage homePage = new HomePage(driver);

        Integer idFromNotesTable = homePage.getIdFromNotesTable();

        if (idFromNotesTable == null) {
            //notes create one
            Note testNote = createTestNote();
            noteService.addNoteForUser(testNote);
            //refresh the site, otherwise the new note is not visible
            driver.navigate().refresh();

            //fetch again
            idFromNotesTable = homePage.getIdFromNotesTable();
        }

        final Integer idFromNotesTableForTest = idFromNotesTable;

        homePage.clickButtonById("nav-notes-tab");
        homePage.clickButtonById("note-btn-delete-" + idFromNotesTable);

        Assertions.assertThrows(NoSuchElementException.class,
                () -> driver.findElement(By.id("note-" + idFromNotesTableForTest)));

    }
    //</editor-fold>

    //<editor-fold desc="Credentials">
    @Test
    @Order(11)
    public void createCredential() {
        login();

        CredentialPage credentialPage = new CredentialPage(driver);
        //navigate to credentials
        credentialPage.navigateCredentialTab();

        //click add credential and fill form
        Credential credential = createTestCredential();
        credentialPage.addCredential(credential);

        //check if table contains a row with the new credential - chek by url, username and password
        Integer credentialId = credentialService.getCredentialId(userService.getUser(userName).getUserId());
        Assertions.assertNotNull(credentialId);
        Assertions.assertEquals(credential.getUrl(), credentialPage.getInnerHTMLForId("credential-url-" + credentialId));
        Assertions.assertEquals(credential.getUserName(), credentialPage.getInnerHTMLForId("credential-username-" + credentialId));

        //encrypt password with existing key
        String encPassword =
                encryptionService.encryptValue(credential.getPassword(),
                        credentialService.getKeyForCredential(credentialId));

        Assertions.assertEquals(encPassword, credentialPage.getInnerHTMLForId("credential-password-" + credentialId));
    }

    @Test
    @Order(12)
    public void updateCredential() {
        login();
        CredentialPage credentialPage=new CredentialPage(driver);
        //Get existing credentials
        List<Credential> credentials =
                credentialService.getCredentialsForUser(userService.getUser(userName).getUserId());
        if(credentials.size()==0){
            //no credential, make one :-)
            createCredential();
            //refresh list
            credentials =
                    credentialService.getCredentialsForUser(userService.getUser(userName).getUserId());
        }

        //get an index
        final Integer randomIndex = NumberHelpers.getRandomNumber(credentials.size() - 1);
        //get the credential and change it
        Credential credentialToChange = credentials.get(randomIndex);
        credentialToChange.setPassword("secret");
        credentialToChange.setUserName("changedUsername");
        credentialToChange.setUrl("http://michgibtesnicht.unsinn");

        credentialPage.updateCredential(credentialToChange);
        //get encryptionKey from database - because it is changed on every update
        final String keyForCredential = credentialService.getKeyForCredential(credentialToChange.getCredentialId());

        //encrypt and compare
        credentialToChange.setPassword(encryptionService
                .encryptValue(credentialToChange.getPassword(),keyForCredential));

        Assertions.assertEquals(credentialToChange.getUrl(),
                credentialPage.getInnerHTMLForId("credential-url-" + credentialToChange.getCredentialId()));
        Assertions.assertEquals(credentialToChange.getUserName(),
                credentialPage.getInnerHTMLForId("credential-username-" + credentialToChange.getCredentialId()));
        Assertions.assertEquals(credentialToChange.getPassword(),
                credentialPage.getInnerHTMLForId("credential-password-" + credentialToChange.getCredentialId()));

    }

    @Test
    @Order(13)
    public void deleteCredential() {
        login();

        CredentialPage credentialPage = new CredentialPage(driver);

        //navigate to credentials
        credentialPage.navigateCredentialTab();
        Integer idToDelete = credentialPage.getIdFromCredentialTable();

        if (idToDelete == null) {
            //no credentials there :-(
            Credential credential = createTestCredential();
            credentialService.addCredential(credential);
            //refresh the site, otherwise the new note is not visible
            driver.navigate().refresh();

            //now we fetch the id for this credential
            idToDelete = credentialPage.getIdFromCredentialTable();
        }

        //click delete credential
        credentialPage.clickButtonById("" + idToDelete);

        //the row marked with the id should have gone :-)
        Integer idToDeleteFinal = idToDelete;
        Assertions.assertThrows(NoSuchElementException.class,
                () -> driver.findElement(By.id("credential-" + idToDeleteFinal)));


    }
    //</editor-fold>


    //<editor-fold desc="Helpers">

    private void login() {
        //log User in
        driver.get(baseUrl + "/login");
        LoginPage loginPage = new LoginPage(driver);
        //check if user is therr
        if (userService.getUser(userName) == null) {
            //create User
            userService.addUser(new User(userName, firstName, lastName, password));
        }
        loginPage.login(userName, password);
        driver.getCurrentUrl();
        Assertions.assertEquals("Home", driver.getTitle());
    }

    private Note createTestNote() {
        String title = makeUniqueTitle();
        return new Note(
                title,
                "this is a note generated by seleniumtest",
                userService.getUser(userName).getUserId());
    }

    private Credential createTestCredential() {
        return new Credential(
                "http://localhost:8080",
                "TestUser",
                null,
                "topsecret",
                userService.getUser(userName).getUserId()
        );
    }

    private String makeUniqueTitle() {
        Date date = new Date();
        return "Test:" + new Timestamp(date.getTime()).getTime();
    }
    //</editor-fold>

}
