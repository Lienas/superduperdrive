package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.model.Credential;
import com.udacity.jwdnd.course1.cloudstorage.services.CredentialService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/credentials")
public class CredentialController {

    Logger logger = LoggerFactory.getLogger(CredentialController.class);

    CredentialService credentialService;
    UserService userService;

    private Integer userId;

    public CredentialController(CredentialService credentialService,
                                UserService userService) {
        this.credentialService = credentialService;
        this.userService = userService;
    }

    @PostMapping
    public RedirectView updateOrCreateNote(@ModelAttribute Credential credential,
                                           RedirectAttributes attributes,
                                           Authentication authentication) {
        //get userId
        userId = userService.getUser(authentication.getName()).getUserId();

        Integer credentialId = credential.getCredentialId();
        if (credentialId == null) {
            //add new credential
            credential.setUserId(userId);
            credentialService.addCredential(credential);
        } else {
            //update existing credential
            //check permissions first
            if (credentialService.getCredentialById(credentialId).getUserId() != userId) {
                attributes.addFlashAttribute("errorMsg",
                        "You are not authorized to change this credential");
            } else {
                if (credentialService.updateCredential(credential) == 0) {
                    attributes.addFlashAttribute("errorMsg",
                            "Sorry, but something went wrong. Pls. try again!");
                } else {
                    attributes.addFlashAttribute("successMsg",
                            String.format(
                                    "Credentials for url: %s were updated successfully 😊",
                                    credential.getUrl())
                    );
                }
            }
        }

        return new RedirectView("/home#nav-credentials-tab");
    }

    @GetMapping("/delete")
    public RedirectView deleteCredential(@RequestParam Integer id,
                                         RedirectAttributes attributes,
                                         Authentication authentication) {

        Credential credential = credentialService.getCredentialById(id);
        if (credential == null) {
            attributes.addFlashAttribute("errorMsg", "no credential with id=" + id + " found!");
            return new RedirectView("/home#nav-credentials-tab");
        }
        //check permission
        if (userService.getUser(authentication.getName()).getUserId() != credential.getUserId()) {
            attributes.addFlashAttribute("errorMsg", "no permission for credential with id=" + id);
            return new RedirectView("/home#nav-credentials-tab");
        }

        if (credentialService.deleteCredential(id) > 0) {
            attributes.addFlashAttribute("successMsg",
                    "Successfully deleted credentials for url: " + credential.getUrl());
        }

        return new RedirectView("/home#nav-credentials-tab");
    }

    /**
     * Async Method for fetching decrypted credential data
     *
     * @param credentialId
     * @param authentication
     * @return JSON-Object
     */
    @GetMapping(value = "/async/getDecryptedCredential", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Map<String, String>> getDecryptedCredential(
            @RequestParam Integer credentialId,
            Authentication authentication) {

        logger.info(String.format("User %s called async service", authentication.getName()));

        Credential credential = credentialService.getCredentialById(credentialId);
        Map<String, String> map = new HashMap();

        //check permission
        if (userService.getUser(authentication.getName()).getUserId() != credential.getUserId()) {
            map.put("errorMsg", "You have no permission for these data !!");
            return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
        }

        credentialService.decryptPassword(credential);

        map.put("id", String.valueOf(credential.getCredentialId()));
        map.put("username", credential.getUserName());
        map.put("url", credential.getUrl());
        map.put("password", credential.getPassword());

        return new ResponseEntity<>(map, HttpStatus.OK);
    }

}
