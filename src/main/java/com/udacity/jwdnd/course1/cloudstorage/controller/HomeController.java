package com.udacity.jwdnd.course1.cloudstorage.controller;
import com.udacity.jwdnd.course1.cloudstorage.model.Credential;
import com.udacity.jwdnd.course1.cloudstorage.model.Note;
import com.udacity.jwdnd.course1.cloudstorage.services.CredentialService;
import com.udacity.jwdnd.course1.cloudstorage.services.FileService;
import com.udacity.jwdnd.course1.cloudstorage.services.NoteService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/home")
public class HomeController {

    private final NoteService noteService;
    private final UserService userService;
    private final FileService fileService;
    private final CredentialService credentialService;

    public HomeController(NoteService noteService,
                          UserService userService,
                          FileService fileService,
                          CredentialService credentialService) {

        this.noteService = noteService;
        this.userService = userService;
        this.fileService = fileService;
        this.credentialService = credentialService;
    }

    @GetMapping
    public String HomeView(Model model, Authentication authentication) {

        String userName = authentication.getName();
        final Integer userId = userService.getUser(userName).getUserId();

        model.addAttribute("welcome", " Welcome " + userName);

        model.addAttribute("Note", new Note());
        model.addAttribute("Credential", new Credential());

        model.addAttribute("notes", noteService.getAllNotesForUser(userId));
        model.addAttribute("credentials", credentialService.getCredentialsForUser(userId));
        model.addAttribute("files",fileService.getAllFilesForUser(userId));

        return "home";
    }

}
