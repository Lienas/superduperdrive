package com.udacity.jwdnd.course1.cloudstorage.mapper;

import com.udacity.jwdnd.course1.cloudstorage.model.Note;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface NoteMapper {
    @Insert("INSERT INTO notes (notetitle, notedescription, userid) " +
            "VALUES (#{noteTitle}, #{noteDescription}, #{userId})")
    @Options(useGeneratedKeys = true,
            keyColumn = "noteId",
            keyProperty = "noteId")
    Integer addNote(Note note);

    @Select("SELECT * FROM notes " +
            "WHERE userid= #{userId}")
    List<Note> fetchNotesforUser(Integer userId);

    @Update("UPDATE notes " +
            "SET notetitle=#{noteTitle}, notedescription=#{noteDescription} " +
           "WHERE noteid = #{noteId}")
    public Integer updateNote(Note note);

    @Select("SELECT userid FROM notes " +
            "WHERE noteid=#{noteId}")
    public Integer getUserForNote(Integer noteId);

    @Delete("DELETE FROM notes " +
            "WHERE noteid = #{noteId}")
    public Integer deleteNote(int noteId);

    @Select("SELECT * FROM notes " +
            "WHERE noteid=#{noteId}")
    public Note getNoteforId(int noteId);

    @Select("SELECT * from NOTES " +
            "WHERE notetitle=#{noteTitle}")
    public List<Note> getNotesForTitle(String noteTitle);
}
