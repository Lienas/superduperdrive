package com.udacity.jwdnd.course1.cloudstorage.pagemodels;

import com.udacity.jwdnd.course1.cloudstorage.helpers.NumberHelpers;
import com.udacity.jwdnd.course1.cloudstorage.model.Credential;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CredentialPage {
    private final WebDriverWait wait;
    private final JavascriptExecutor js;

    public CredentialPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.wait = new WebDriverWait(driver, 2);
        js = (JavascriptExecutor) driver;
    }

    @FindBy(id = "add-credential-btn")
    WebElement addCredentialBtn;

    @FindBy(id = "submit-credential-btn")
    WebElement submitCredentialBtn;

    @FindBy(id = "credential-url")
    WebElement credentialUrlField;

    @FindBy(id = "credential-username")
    WebElement credentialUsernameField;

    @FindBy(id = "credential-password")
    WebElement credentialPasswordField;

    @FindBy(id = "credentialTable")
    private WebElement credentialTable;

    public void navigateCredentialTab() {
        clickButtonById("nav-credentials-tab");
    }

    public void clickButtonById(String id) {
        WebElement element = wait.until(driver -> driver.findElement(By.id(id)));
        js.executeScript("arguments[0].click()", element);
    }

    public void addCredential(Credential credential) {
        js.executeScript("arguments[0].click()", addCredentialBtn);
        addOrUpdateCredential(credential);
    }

    public void updateCredential(Credential credentialToChange) {
        clickButtonById("credential-btn-edit-" + credentialToChange.getCredentialId());
        addOrUpdateCredential(credentialToChange);
    }

    private void addOrUpdateCredential(Credential credential) {
        js.executeScript("arguments[0].value='" + credential.getUrl() + "'", credentialUrlField);
        js.executeScript("arguments[0].value='" + credential.getUserName() + "'", credentialUsernameField);
        js.executeScript("arguments[0].value='" + credential.getPassword() + "'", credentialPasswordField);
        js.executeScript("arguments[0].click()", submitCredentialBtn);
    }

    public String getInnerHTMLForId(String id) {
        return wait.until(driver -> driver.findElement(By.id(id)).getAttribute("innerHTML"));
    }

    //this an an option for fetching existing credential
    //which approach is better practice for testing ?
    public Integer getIdFromCredentialTable() {
        //get all child-Elements of the table
        List<WebElement> rows = credentialTable
                .findElement(By.tagName("tbody"))
                .findElements(By.tagName("tr"));

        if (rows.size() == 0) return null;

        //get random element of list
        int randomElement = NumberHelpers.getRandomNumber(rows.size() - 1);
        String idAttr = rows.get(randomElement).getAttribute("id");

        //parse id from id-string credential-[id]
        idAttr = idAttr.substring(idAttr.indexOf('-') + 1);

        return Integer.parseInt(idAttr);
    }
}
